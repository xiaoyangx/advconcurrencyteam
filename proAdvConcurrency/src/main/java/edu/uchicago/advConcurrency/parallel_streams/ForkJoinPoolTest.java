package edu.uchicago.advConcurrency.parallel_streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by xiaoyangxu
 *
 * Avoid shared thread pool by creating own thread pools
 * and allocate more threads than processor cores
 */
public class ForkJoinPoolTest {

    public static void main(String args[]) throws Exception {
        ForkJoinPoolTest test = new ForkJoinPoolTest();
        long startTime = System.nanoTime();
        test.executeTasks();
        System.out.println("Time = " + (System.nanoTime() - startTime) + " ms");
    }

    void executeTasks() throws Exception {
        final List<Integer> firstRange = buildIntRange();
        final List<Integer> secondRange = buildIntRange();

        ForkJoinPool forkJoinPool = new ForkJoinPool(4);
        forkJoinPool.submit(() -> {
            firstRange.parallelStream().forEach((number) -> {
                try {
                    //System.out.println(Thread.currentThread().toString() + number);
                    Thread.sleep(2);
                } catch (InterruptedException e) { }
            });
        });

        ForkJoinPool forkJoinPool2 = new ForkJoinPool(4);
        forkJoinPool2.submit(() -> {
            secondRange.parallelStream().forEach((number) -> {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                }
            });
        });

        // view dump threads
//        Thread.sleep(20_000);
    }


    private List<Integer> buildIntRange() {
        List<Integer> numbers = new ArrayList<>();
        for (int i=0; i<50_000; i++)
            numbers.add(i);
        return Collections.unmodifiableList(numbers);
    }
}