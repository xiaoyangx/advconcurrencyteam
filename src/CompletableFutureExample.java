import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;

/**
 * Created by tregan on 8/18/2016.
 */
public class CompletableFutureExample {

    public static void main(String[] args) throws Exception {
        String expectedValue = "the expected value";
        CompletableFuture<String> alreadyCompleted = CompletableFuture.completedFuture(expectedValue);
        System.out.println("Creating an already completed CompletableFuture:\n");
        System.out.println(alreadyCompleted.get() + " == " + expectedValue);
        System.out.println();
        System.out.println("--------");



        CompletableFuture completableFuture1 = new CompletableFuture();

        new Thread (()-> {
            try {
                Thread.sleep(4000L);
            } catch (Exception e) {
                completableFuture1.complete(-100.0);
            }
            completableFuture1.complete(100.0);
        },"CompFut1-Thread").start();
        System.out.println("ok...waiting at: "+new Date());
        System.out.format("compFut value and received at: %f, %s \n", completableFuture1.join(), new Date());

    }
}
