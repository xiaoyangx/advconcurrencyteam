package edu.uchicago.advConcurrency.parallel_streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by xiaoyangxu
 *
 * Execute two parallel streams in parallel
 */
public class TwoStreamTest {

    public static void main(String args[]) throws Exception {
        TwoStreamTest test = new TwoStreamTest();
        long startTime = System.nanoTime();
        test.executeTasks();
        System.out.println("Time = " + (System.nanoTime() - startTime) + " ms");
    }

    void executeTasks() throws Exception {
        final List<Integer> firstRange = buildIntRange();
        final List<Integer> secondRange = buildIntRange();


        Runnable firstTask = () -> {
            firstRange.parallelStream().forEach((number) -> {
                try {
                    // do something slow
                    //System.out.println(Thread.currentThread().toString() + number);
                    Thread.sleep(2);

                } catch (InterruptedException e) { }
            });
        };

        Runnable secondTask = () -> {
            secondRange.parallelStream().forEach((number) -> {
                try {
                    // do something slow
                    Thread.sleep(2);
                } catch (InterruptedException e) { }
            });
        };

        Thread t1 = new Thread(firstTask);
        Thread t2 = new Thread(secondTask);
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }

    private List<Integer> buildIntRange() {
        List<Integer> numbers = new ArrayList<>(5);
        for (int i=0; i<50_000; i++)
            numbers.add(i);
        return Collections.unmodifiableList(numbers);
    }
}
