package edu.uchicago.advConcurrency.parallel_streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * Created by xiaoyangxu
 *
 * Get prime numbers using parallel streams
 */
public class Prime {

    public static void main(String args[]) throws Exception {
        Prime test = new Prime();
        test.executeTasks();
    }

    private void executeTasks() throws Exception {
        final List<Integer> candidates = buildIntStream();
        final ForkJoinPool forkJoinPool = new ForkJoinPool(5);
        final Set<String> workerThreadNames = new ConcurrentSkipListSet<>();
        final long start = System.currentTimeMillis();

        // stream
        candidates.stream()
                .filter(Prime::isPrime)
                .peek(n -> workerThreadNames.add(Thread.currentThread().getName()))
                .collect(Collectors.toList());
        // parallelStream
//        candidates.parallelStream()
//                .filter(Prime::isPrime)
//                .peek(n -> workerThreadNames.add(Thread.currentThread().getName()))
//                .collect(Collectors.toList());
        // own thread pools
//        forkJoinPool.submit(() -> candidates.parallelStream()
//                .filter(Prime::isPrime)
//                .peek(n -> workerThreadNames.add(Thread.currentThread().getName()))
//                .collect(Collectors.toList())).get();

        System.out.println("Execution time: " + (System.currentTimeMillis() - start) + " msecs");
        System.out.println("Threads: ");
        workerThreadNames.forEach((thread) -> System.out.println(thread));
    }

    // data source
    private List<Integer> buildIntStream() {
        List<Integer> numbers = new ArrayList<>();
        for (int i=0; i<5_000_000; i++)
            numbers.add(i);
        return numbers;
    }

    // check prime number
    private static boolean isPrime(int candidate) {
        for (int i = 2; i * i <= candidate; ++i) {
            if (candidate % i == 0) {
                return false;
            }
        }
        return true;
    }
}
