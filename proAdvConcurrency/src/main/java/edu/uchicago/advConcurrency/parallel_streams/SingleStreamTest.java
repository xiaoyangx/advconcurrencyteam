package edu.uchicago.advConcurrency.parallel_streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by xiaoyangxu
 *
 * With only one stream
 */
public class SingleStreamTest {

    public static void main(String args[]) throws Exception {
        SingleStreamTest test = new SingleStreamTest();
        long startTime = System.nanoTime();
        test.executeTasks();
        System.out.println("Time = " + (System.nanoTime() - startTime) + " ms");
    }

    void executeTasks() throws Exception {
        final List<Integer> firstRange = buildIntRange();

        firstRange.parallelStream().forEach((number) -> {
            try {
                // do something slow
                Thread.sleep(5);
            } catch (InterruptedException e) { }
        });
    }

    private List<Integer> buildIntRange() {
        List<Integer> numbers = new ArrayList<>(5);
        for (int i=0; i<50_000; i++)
            numbers.add(i);
        return Collections.unmodifiableList(numbers);
    }
}